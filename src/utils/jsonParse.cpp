#include "../utils/jsonParse.h"

String jsonParseWordStr(String jsonData, String jsonKey) {
  /*  Json Data format is - (Key):"(Data)"  */
  int parseStartIndex = 0, parseFromIndex = 0;
  int parseWordSize = jsonKey.length() + 2; //  jsonKey 및 parse 문자 검색 인덱스에서 제외하고 검색 (parseWordSize = jsonKey len + 2 (':', '"'의 len)) 

  jsonKey = jsonKey + ":\"";
  parseStartIndex = jsonData.indexOf(jsonKey,parseStartIndex);
  parseFromIndex = jsonData.indexOf("\"", parseStartIndex + parseWordSize);
  return jsonData.substring(parseStartIndex + parseWordSize, parseFromIndex);
} 

String jsonParseWordNum(String jsonData, String jsonKey) {
  /*  Json Data format is - (Key):"(Data)"  */
  int parseStartIndex = 0, parseFromIndex = 0;
  int parseWordSize = jsonKey.length() + 2; //  jsonKey 및 parse 문자 검색 인덱스에서 제외하고 검색 (parseWordSize = jsonKey len + 2 (':', '"'의 len)) 

  parseStartIndex = jsonData.indexOf(jsonKey,parseStartIndex);
  parseFromIndex = jsonData.indexOf(",", parseStartIndex + parseWordSize);
  return jsonData.substring(parseStartIndex + parseWordSize, parseFromIndex);
} 