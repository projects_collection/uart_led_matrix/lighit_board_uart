#include <Arduino.h>

#ifndef _JSONPARSE_
#define _JSONPARSE_

#define JSON_PARSE_WORD_SIZE 4          // JSON_PARSE_WORD_SIZE = JSON Pasring Data : ""      
#define WIFI_JSON_KEY_SIZE  10   // WIFI_JSON_KEY_SIZE = JSON Pasring Key I, D, :, ", ", P, W, :, ", "

String jsonParseWordStr(String jsonData, String jsonKey);    //  "문자열 인 경우 "" 문자 제거
String jsonParseWordNum(String jsonData, String jsonKey);    //  "" 콤마 없는 Num 경우 바로 정수 그대로 가져옴

#endif