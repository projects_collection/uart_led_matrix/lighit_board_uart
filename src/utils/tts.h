#include <Arduino.h>

#ifndef _TTS
#define _TTS

#define VOICE_TERM  1000

#define TTS_PLAY_COUNT                  1

#define TTS_TEXT_PAGE_INFO_TOTAL_COUNT  9
#define TTS_TEXT_PAGE_INFO_COUNT_IDX    8

void audioTimerBegin();
void audioTimerStop();
void initSoundFile(uint8_t* wavByteData);  
void setWavFile();  
void deleteSoundObj();
void waySoundFilePlay();

#endif