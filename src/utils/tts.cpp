#include "../../popsign.h"
#include "../utils/tts.h"
#include "../utils/etc.h"
#include "../board/config.h"

//  Board Config Info 
extern Board_Config g_board_config;

uint8_t g_soundPlayCurCount = 0;    //  현재 Play Count 저장 변수
uint8_t g_soundPlayTotalCount = 2;  //  총 Play Count 저장 변수

// #if (MODEL_INFO == VOICE_MODEL)
  #include <XT_DAC_Audio.h>
  XT_Wav_Class* Sound = NULL;
  XT_DAC_Audio_Class DacAudio(25,0);  

  /** 
   * @fn      - audioTimerBegin()
   * @brief   - DacAudio 객체 Timer Begin
  */
  void audioTimerBegin() {
    DacAudio.beginTimer();
  }

   /** 
   * @fn      - audioTimerStop()
   * @brief   - DacAudio 객체 Timer Stop
  */
  void audioTimerStop() {
    DacAudio.stopTimer();
  }


  /** 
   * @fn      - initSoundFile()
   * @brief   - 재생 할 웨이브 파일 생성자 인수로 XT_Wav_Class 함수 할당 
  */
  void initSoundFile(uint8_t* wavByteData) {
    deleteSoundObj();
    Sound = new XT_Wav_Class(wavByteData);  
    DacAudio.Play(Sound);
  }


  /** 
   * @fn      - setWavFile()
   * @brief   - 웨이브 파일 등록
   *          - 1회 재생 후 다시 등록해줘야 반복 플레이 가능함.
  */
  void setWavFile() {
    audioTimerBegin();
    DacAudio.Play(Sound);
  }

  /** 
   * @fn      - deleteSoundObj()
   * @brief   -  웨이브 파일 객체 메모리 해제
  */
  void deleteSoundObj() {
    if (Sound != NULL) 
    {
      delete Sound;
      Sound = NULL;
    }
  }

  /** 
   * @fn      - waySoundFilePlay()
   * @brief   - 웨이브 파일 재생
   *          - Loop 반복하며 wayFile 재생
  */
  void waySoundFilePlay() {
    // Serial.println("waySoundFilePlay");        
    DacAudio.FillBuffer();   //  Setting 된 Wav 파일 재생하는 함수             
  }
// #endif
