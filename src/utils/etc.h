#ifndef _ETC_
#define _ETC_

#define BUSY_WAITS  1
#define NON_BUSY    0

boolean delayExt(unsigned long cur_time, unsigned long * pre_time, uint16_t inteval);
int get_critical_status(void);
void critical_section_begin(void);
void critical_section_end(void);
#endif