#ifndef _DISPLAY_PAGE_
#define _DISPLAY_PAGE_

// 페이지 간격 시간
#define DEFALUT_PAGE_INTERVAL_TIME 100
// 페이지 유지 시간
#define DEFALUT_PAGE_TIME 5000

// Lora Sub Mode - 페이지 출력 MODE
#define LORA_CONTENT_PAGE_PRINT_MODE 0
// Lora Sub Mode - 게이지 출력 MODE
#define LORA_CONTENT_GAGE_PRINT_MODE 1
//  Lora Defalut 페이지 
#define LORA_DEFALUT_PAGE 99


bool nextPagePrint(void* matrix, LedPannel* ledpannel, bool * isSensorPage);
bool checkNextPageCondition(int _displayPageTime, LedPannel* ledpannel);
bool checkTimerPagePrint(void* matrix, LedPannel* ledpannel);
int init_multiPage();
void free_multiPage();
void play_display(void* matrix, LedPannel* ledpannel);
void random_page_print(void* matrix, LedPannel* ledpannel);
void random_gage_print(void* matrix, LedPannel* ledpannel);

#endif