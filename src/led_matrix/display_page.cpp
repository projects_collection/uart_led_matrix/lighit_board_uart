#include <FS.h>
#include "../../popsign.h"
#include "../led_matrix/ledpannel.h"
#include "../led_matrix/display_page.h"
#include "../utils/etc.h"
#include "../utils/tts.h"
#include "../communication/command.h"
#include "../flash/flash.h"
#include "../img/bitmap_mode_logo.h"
#include "../board/board.h"

//  Board Config Info 
extern Board_Config g_board_config;

/*  디스플레이 모드 관련 변수  */
extern char* pageBuffer[PAGE_TOTAL_COUNT];
extern size_t pageSize[PAGE_TOTAL_COUNT];
extern int8_t g_pageCount;
extern bool nextPageToken;
extern bool autoNextPageMode;
extern bool categoryActionFlag;
extern bool timeSwitchIsEnable[PAGE_TOTAL_COUNT];
extern bool g_is_exist_sensor_page;  // 5페이지 중에 센서페이지가 있는지 체크하는 변수
extern int displayPageTime;
extern int displayPageIntervalTime;
extern bool isSinglePagePrint;  // 단일 페이지일 경우 깜빡거리지 않게 하기 위해 설정
extern uint8_t display_step;  //  디스플레이 모드 출력 스텝
extern int int_flg;
extern uint8_t pageEnableList[PAGE_TOTAL_COUNT];  // 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
extern uint8_t* gif_background_page_buffer[PAGE_TOTAL_COUNT]; //  gif 배경 버퍼
extern size_t gif_background_page_size[PAGE_TOTAL_COUNT];  //  gif 배경 버퍼 크기
unsigned long page_interval_pre_time; //  페이지 전환 시간 간격 저장 값

extern bool mIsGifPlay;  //  gif play status (다중 스레드 공유 변수 Flag)

/* TTS 관련 변수 */
extern uint8_t* tts_wav_page_buffer[PAGE_TOTAL_COUNT];
extern uint8_t g_soundPlayCurCount;
extern uint8_t g_soundPlayTotalCount; 

/*  Mode 관련 변수 */
extern int logoTime;

/* Sensor 관련 변수 */
bool g_is_sensor_mode = false;
bool g_is_exist_sensor_page = false;  // 5페이지 중에 센서페이지가 있는지 체크하는 변수

/* Wifi 모드 관련 변수 */
extern bool isStreamCallback;

//  Gage Mode 관련 변수 - 게이지 갯수
extern int g_gage_count;

/**
 * 디스플레이 모드 다음 페이지 검사 후 출력하는 함수
 * 센서 페이지 검사하여 표시
 * 경우 1 : 액션인 경우 -> 액션의 루틴의 1회가 끝난 후 nextPageToken 값에 따른 페이지 전환
 * 경우 2 : 디폴트 인경우 -> 지정된 시간에 따라 페이지 전환
 * 경우 3 : 수동 모드 인경우 -> 모드 스위치 전환으로 페이지 전환  
 * 
 **/
bool nextPagePrint(void* matrix, LedPannel* ledpannel, bool * isSensorPagePrint) {
    for (int i = 0; i < (PAGE_TOTAL_COUNT); i++) {  //  -1 == 현재 페이지 제외한 페이지까지 반복 
      // 다음 페이지가 있을 때 까지 페이지를 넘김.
      g_pageCount++;
      // PRINTF("g_pageCount : %d \n", g_pageCount);
      if (g_pageCount >= PAGE_TOTAL_COUNT) {
        if (*isSensorPagePrint) {
          // PRINTF("g_pageCount >= PAGE_TOTAL_COUNT \n");
          *isSensorPagePrint = false;
          ledpannel->matrixClear();
        }
        g_pageCount = 0; 
      }
      // if 페이지가 있다면
      if (pageSize[(g_pageCount)]) {    

        // 현재 페이지가 Timer 설정이 되어있는 페이지인 경우 
        if (timeSwitchIsEnable[g_pageCount] && autoNextPageMode) { 
          //  현재 시간이 페이지 출력 시간이 아닌 경우 continue (다음 페이지 출력)
          if (!checkTimerPagePrint(matrix, ledpannel)) continue;  
          return true;
        } else { // 시간 조건 설정이 되어있지 않은 경우    
          ledpannel->multiPagePrint(g_pageCount); 
          page_interval_pre_time = millis();
          return true;
        }
      } 
    }

    return false;
}

/**
 * @fn checkTimerPage
 * @brief Timer 설정 된 페이지 Time 검사하여 페이지 출력
 */ 
bool checkTimerPagePrint(void* matrix, LedPannel* ledpannel) {
  int timer_switch_result;
  timer_switch_result = timer_switch_print_page();

  if (timer_switch_result == PAGE_TIME_ASYNC) {
    // if timer_switch_print_page() 반환 값이 시간 동기화가 되어있지 않는 경우 아래 메시지 출력
    ledpannel->print_str_matrix_basic(FONT_ONE_SIZE, "시간 설정 오류\n블루투스 연결 필요", 0x02, ((MATRIX*)matrix)->color565(150,150,150));
    page_interval_pre_time = millis();
    return true;
  } else if (timer_switch_result == PAGE_TIME_OK) {
    // if timer_switch_print_page() 반환 값에 현재 시간이 페이지 출력 시간인 경우 정상 페이지 출력
    ledpannel->multiPagePrint(g_pageCount); 
    page_interval_pre_time = millis();
    return true;
  } else if (timer_switch_result == PAGE_TIME_NO) {
    return false;
  }
}

/**
 * 다음페이지까지 넘기기 위한 조건 검사
 * 경우 1 : 액션인 경우 -> 액션의 루틴의 1회가 끝난 후 nextPageToken 값에 따른 페이지 전환
 * 경우 2 : 디폴트 인경우 -> 지정된 시간에 따라 페이지 전환
 * 경우 3 : 수동 모드 인경우 -> 모드 스위치 전환으로 페이지 전환  
 * 
 * autoNextPageMode : 디스플레이 페이지 수동 변환 모드
 * nextPageToken : 액션 루틴 끝난 후 페이지 토큰 true 반환
 **/
bool checkNextPageCondition(int _displayPageTime, LedPannel* ledpannel) {
  bool _next_page_ok = false;

  if (mIsGifPlay) return false; //  현재 다른 코어에서 Gif 실행중인경우 return

  if (autoNextPageMode) { // 자동 페이지 전환 모드
    if (categoryActionFlag == true ) {  //  페이지 내용이 액션이라면 nextPageToken == true 다음 페이지 전환(한 번의 액션이 끝났을 때 true 반환하는 함수)
      if (nextPageToken == true) {
        _next_page_ok = true;
      } else {
        _next_page_ok = false;
      }
    } else {  //  페이지 내용이 액션이 아니라면 시간에 따라 페이지 전환
      if (delayExt(millis(), &page_interval_pre_time, _displayPageTime)) {
        _next_page_ok = true;
      } else _next_page_ok = false;
    }
  } else {  //  수동 페이지 전환 모드
    if (int_flg == PAGE_CHANGE) { 
      int_flg = 0;
      _next_page_ok = true;
    } else _next_page_ok = false;
  }

  if (MODEL_INFO == VOICE_MODEL) {
    if (ledpannel->m_tts_function_Flag && _next_page_ok == true) {  // 다음 페이지 조건 충족 및 tts 재생 중인 경우
      if (g_soundPlayCurCount < g_soundPlayTotalCount) {  //  totalCount만큼 반복 출력
        PRINTF("g_soundPlayCurCount : %d \n", g_soundPlayCurCount);
        setWavFile();
        g_soundPlayCurCount++;
        nextPageToken = false;
        page_interval_pre_time = millis();
        return false;
      } 
      else {
          deleteSoundObj();
        return true;
      }
    }
  }
  
  return _next_page_ok; //  다음 페이지 전환 여부 return;
}
/**************************************************************************/
/** 
 * @brief   - 페이지 로드 함수 (플래시 -> SRAM)
 * @details - page1~5 로드 -> pageBuffer[0~4]
*/
/**************************************************************************/
int init_multiPage() {
  File pageFile;    //  페이지 파일
  int pageTotalCount = 0;
  char pageStr[30];

  for(int i = 0; i < PAGE_TOTAL_COUNT; i++) {
    if (pageEnableList[i] == FLASH_PAGE_ENABLE_VALUE) {

      sprintf(pageStr, "/page%d.txt", i + 1); //현재 0페이지로 들어오게 되어있어서 0 + 1 연산
      pageFile = LittleFS.open(pageStr);
      pageSize[i] = pageFile.size();

      //  Page1~20 Flash Byte Read
      if(pageSize[i]){ 
        pageTotalCount++;
      }
      pageFile.close();
    } else {
      pageSize[i] = 0;
    }
  }
  return pageTotalCount;
}

/**************************************************************************/
/** 
 * @brief   - 페이지 버퍼 배열 메모리 초기화 함수
 * @details - pageBuffer[0~4] = null
*/
/**************************************************************************/
void free_multiPage()
{
  for (int i = 0; i < PAGE_TOTAL_COUNT; i++) {
    pageSize[i] = 0;
    if (pageBuffer[i] != NULL) { 
      free(pageBuffer[i]);
      pageBuffer[i] = NULL;
    }
    if (tts_wav_page_buffer[i] != NULL) { 
      tts_wav_page_buffer[i] = 0;
      free(tts_wav_page_buffer[i]);
      tts_wav_page_buffer[i] = NULL;
    }
    if (gif_background_page_buffer[i] != NULL) { 
      free(gif_background_page_buffer[i]);
      gif_background_page_buffer[i] = NULL;
    }
  }
}

/**************************************************************************/
/** 
 * @fn  - play_display()
 * @brief   - 디스플레이 모드 출력
 * @details - display_step == 0 -> 페이지 초기화 및 첫 페이지출력
 *            display_step == 1 -> 다음 페이지 조건 검사 및 페이지 출력
 *            display_step == 2 -> 단일 페이지 조건 검사 및 페이지 출력
*/
/**************************************************************************/
void play_display(void* matrix, LedPannel* ledpannel) {
  // PRINTF("display_step : %d \n", display_step);
  if (display_step == 0) 
  {

    // 페이지가 있다면 AND 현재 출력 중인 페이지가 있다면 끝나고 페이지 작업 수행
    // 와이파이 모드 경우 출력 중에 Stream 들어오면 출력 끝난 후 페이지 리스트 갱신
    for (int i = 0; i < PAGE_TOTAL_COUNT; i++) {
      if (pageSize[i]) { 
        if (!checkNextPageCondition(displayPageTime, ledpannel)) {
          ledpannel->command_excute();
          return;  
        }  
      }
    }
    char pageStr[10];
    int pageTotalCount = 0;
    g_pageCount = -1; //  센서 감지 시 첫 페이지부터 실행하기 위하여 -1으로 이동

    // init_modeChange_flag();
    pageTotalCount = init_multiPage();  // 
    // PRINTF("pageTotalCount : %d \n", pageTotalCount);
    if (pageTotalCount == 0) {  //  페이지 없는 경우 페이지 없음 이모지 출력
      uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
      display_step = 2;
      ((MATRIX*)matrix)->drawRGBBitmap(x_pos_offset, 0, (uint16_t*) non_page_64_32, 64, 32);
      return;
    } else if (pageTotalCount == 1 && !g_is_exist_sensor_page) { //  페이지가 하나인 경우 모드스탭 3으로 이동
      nextPagePrint(((MATRIX*)matrix), ledpannel, &g_is_sensor_mode);
      display_step = 2;
      return;
    }
    display_step = 1;

    /*  첫 페이지 출력 후 검사  */
    nextPagePrint(((MATRIX*)matrix), ledpannel, &g_is_sensor_mode);
  }
  else if (display_step == 1) {
  // 페이지 조건에 따라 전환되며 출력하는 영역 
    if (!g_is_sensor_mode && g_is_exist_sensor_page) {
      if (digitalRead(SENSOR_PIN) != 0) {
        PRINTF("센서 감지! \n");
        g_is_sensor_mode = true;
        g_pageCount = -1;
        nextPagePrint(((MATRIX*)matrix), ledpannel, &g_is_sensor_mode);
      }
    }
    
    ledpannel->command_excute(); 
    if (checkNextPageCondition(displayPageTime, ledpannel)) {
        categoryActionFlag = false;
        ledpannel->set_action_off(false);
        nextPageToken = false;
        page_interval_pre_time = millis();
        if (pageBuffer[g_pageCount] != NULL) {  //  다음 페이지로 전환 시 Page Data 해제
          free(pageBuffer[g_pageCount]);
          pageBuffer[g_pageCount] = NULL;
        }
    } else return;
  

    if (!nextPagePrint(((MATRIX*)matrix), ledpannel, &g_is_sensor_mode)) {
      // 마지막 페이지까지 page 출력이 없었다면 다음 페이지 조건 검사하지 않고 다음 바로 페이지 실행
      page_interval_pre_time = 0;
    }
  }
  else if (display_step == 2) {
    if (isStreamCallback) { //  와이파이 모드 경우 처리 ()
      if (!checkNextPageCondition(displayPageTime, ledpannel)) {
        ledpannel->command_excute();
        return;  
      } 
      nextPagePrint(((MATRIX*)matrix), ledpannel, &g_is_sensor_mode);
      isStreamCallback = false;
    }
    else if (ledpannel->m_tts_function_Flag) {   //  TTS 모드 경우 반복 처리 ()
      checkNextPageCondition(displayPageTime, ledpannel);
      g_soundPlayCurCount = 0;
    }
    
    ledpannel->command_excute(); //  단일 페이지 or 페이지 없음
    //단일 페이지일 경우 시간 검사 (페이지 시간 설정 값에 의해 변경 있을 시 1회 On/Off)
    if (timeSwitchIsEnable[g_pageCount]) {
      int timer_switch_result = timer_switch_print_page();
      if (timer_switch_result == PAGE_TIME_ASYNC) {
        if (isSinglePagePrint) {  //  한번만 출력하고  command_excute 돌게 설정
          ledpannel->print_str_matrix_basic(FONT_ONE_SIZE, "시간 설정 오류\n블루투스 연결 필요", CMD_ACT_RIGHT_LEFT, ((MATRIX*)matrix)->color565(150,150,150));
          isSinglePagePrint = false;
        }
      } else if (timer_switch_result == PAGE_TIME_OK) {
        if (!isSinglePagePrint) {
          ((MATRIX*)matrix)->clearScreen();
          ledpannel->multiPagePrint(g_pageCount); 
          page_interval_pre_time = millis();
          return;
        }
      } else if (timer_switch_result == PAGE_TIME_NO) {
        if (isSinglePagePrint) {
          ledpannel->set_action_off();
        }
      }
    }
  }
}

/**
 * @brief randoe page print
 * @details 저장된 페이지중에 랜덤 페이지 출력
 * @param matrix 
 * @param ledpannel 
 */
void random_page_print(void* matrix, LedPannel* ledpannel) {
  if (!nextPageToken) {
    Serial.println("random_page_print retrn!!");
    return;   
  }
  Serial.println("random_page_print ok!!");

  int _randNumber;  //  Total Page Count 에 대한 Random Value
  int _pageTotalCount = 0;  //  Total Page Count
  int _check_page_index = 0;  //  Random Value 연속되지 않는 페이지 처리를 위한 변수 

  //Note - -1 is 100페이지 현재 Deflaut 페이지
  for(int i = 0; i < PAGE_TOTAL_COUNT - 1; i++) { //  Total Page Count 가져오기 위한 Loop 
      //  Page Flash Byte Read
      if(pageEnableList[i]){ 
        _pageTotalCount++;
      }
  }
  if (_pageTotalCount == 0) return; //  ------------ 페이지 없는 경우 Return---------------

  _randNumber = random(_pageTotalCount);

  //Note - -1 is 100페이지 현재 Deflaut 페이지
  for(int i = 0; i < PAGE_TOTAL_COUNT - 1; i++) { //  저장되어 있는 페이지안에서 Random 값 추출
      if(pageEnableList[i]){ 
        if (_check_page_index == _randNumber) {
          _randNumber = i; 
          break;
        }
        _check_page_index++;
      } 
  }
  if (displayPageIntervalTime > 10000) displayPageIntervalTime = 10000; // 페이지 출력 간격 시간 최대 10초로 제한
  
  delay(displayPageIntervalTime);  //  페이지 간격 시간
  g_pageCount = _randNumber;
  page_interval_pre_time = millis();
  ledpannel->multiPagePrint(_randNumber); 
}

/**
 * @brief randoe gage print
 * @details 수치에 따른 랜덤 게이지 출력
 * @param matrix 
 * @param ledpannel 
 */
void random_gage_print(void* matrix, LedPannel* ledpannel) {
  Serial.println("random_gage_print");
  if (!nextPageToken) {
    Serial.println("random_page_print retrn!!");
    return;   
  }
  ledpannel->matrixClear();

  int _randNumber = random(g_gage_count);  //  Total Page Count 에 대한 Random Value
  _randNumber += 1; //  1 is zero index numbering

  int widthSize = ((float) IDE_MATRIX_WIDTH / g_gage_count);  //  막대 하나당 Width Size (전체 넓이 / 막대 갯수)
  widthSize -= 2;
  int draw_idx = 0;

  for (int i = 0; i < _randNumber; i++) { // for -> 막대 갯수
    draw_idx++;
    uint8_t r = 0, g = 0,b = 0;

    if ((g_gage_count * 0.33) > i) {  //  1/3 지점까지의 막대 색상 빨강
      g = 255;
    } else if ((g_gage_count * 0.66) > i) { //  2/3 지점까지의 막대 색상 주황
      r = 255;
      g = 128;
    } else {  //  그 외 막대 색상 빨강
      r = 255;
    }

    for (int j = 0; j < widthSize; j++) { //  for -> Draw While
      ((MATRIX*)matrix)->writeFastVLine(draw_idx++, 1 ,IDE_MATRIX_HEIGHT - 2,((MATRIX*)matrix)->color565(r, g, b));
    }
    draw_idx++;
  }
  
  nextPageToken = false;
  page_interval_pre_time = millis();
}