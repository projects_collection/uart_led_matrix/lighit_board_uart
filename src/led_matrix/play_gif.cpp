#include <AnimatedGIF.h>
#include <FS.h>
#include <LittleFS.h>
#include "../board/config.h"
#include "../led_matrix/play_gif.h"
#include "../led_matrix/ledpannel.h"
#include "../communication/buffer_serial.h"
#include "../utils/etc.h"

/* GIF 출력 관련 변수 */
extern AnimatedGIF gif;
File f;

int g_gif_play_x_offset = 0, g_gif_play_y_offset = 0; //  Gif 출력 시작 좌표

bool mIsGifPlay = false;  //  gif play status (공유 변수 Flag)

/*  현재 로테이션 값  */
extern uint8_t g_rotationValue;

/*  디스플레이 모드 관련 변수  */
extern bool nextPageToken;
extern bool categoryActionFlag;
extern char* pageBuffer[PAGE_TOTAL_COUNT];
extern size_t pageSize[PAGE_TOTAL_COUNT];
extern int displayPageTime;
extern unsigned long page_interval_pre_time;  //  페이지 전환 시간 간격 저장 값

extern uint8_t* gif_background_page_buffer[PAGE_TOTAL_COUNT]; //  gif 배경 버퍼

extern size_t gif_background_page_size[PAGE_TOTAL_COUNT];  //  gif 배경 버퍼 크기

extern MATRIX *matrix;
extern LedPannel ledpannel;

/*  Multi Core Gif */
TaskHandle_t TaskGifCore1;  

// Task 중복 되지 않기 위한 delay tick
unsigned long task_delay_tick = 0;

/**************************************************************************/
/** 
 * @brief   - GIFDRAW Callback함수
 * @details - 콜백함수 호출 시기에 matrix에 그려주는 함수
*/
/**************************************************************************/

void GIFDraw(GIFDRAW *pDraw)
{
  // Serial.println("GIFDraw");
    uint8_t *s;
    uint16_t *d, *usPalette, usTemp[320];
    int x, y, iWidth;

  iWidth = pDraw->iWidth;
  if (iWidth > 64)
      iWidth = 64;

    usPalette = pDraw->pPalette;
    y = pDraw->iY + pDraw->y; // current line
    
    s = pDraw->pPixels;
    if (pDraw->ucDisposalMethod == 2) // restore to background color
    {
      for (x=0; x<iWidth; x++)
      {
        if (s[x] == pDraw->ucTransparent)
           s[x] = pDraw->ucBackground;
      }
      pDraw->ucHasTransparency = 0;
    }
    // Apply the new pixels to the main image
    if (pDraw->ucHasTransparency) // if transparency used
    {
      uint8_t *pEnd, c, ucTransparent = pDraw->ucTransparent;
      int x, iCount;
      pEnd = s + pDraw->iWidth;
      x = 0;
      iCount = 0; // count non-transparent pixels
      while(x < pDraw->iWidth)
      {
        c = ucTransparent-1;
        d = usTemp;
        while (c != ucTransparent && s < pEnd)
        {
          c = *s++;
          if (c == ucTransparent) // done, stop
          {
            s--; // back up to treat it like transparent
          }
          else // opaque
          {
             *d++ = usPalette[c];
             iCount++;
          }
        } // while looking for opaque pixels
        if (iCount) // any opaque pixels?
        {
          for(int xOffset = 0; xOffset < iCount; xOffset++ ) {
            uint16_t _rotatedX, _rotatedY;
            _rotatedX = x + xOffset + g_gif_play_x_offset;
            _rotatedY = y + g_gif_play_y_offset;

            getRotationPos(&_rotatedX, &_rotatedY, g_rotationValue);
            ledpannel.drawPixelColorCheck(_rotatedX, _rotatedY, usTemp[xOffset], OVER_WRITE_DRAW_FLAG);
            // dma_display.drawPixel(x + xOffset, y, usTemp[xOffset]); // 565 Color Format
          }
          x += iCount;
          iCount = 0;
        }
        // no, look for a run of transparent pixels
        c = ucTransparent;
        while (c == ucTransparent && s < pEnd)
        {
          c = *s++;
          if (c == ucTransparent)
             iCount++;
          else
             s--; 
        }
        if (iCount)
        {
          x += iCount; // skip these
          iCount = 0;
        }
      }
    }
    else // does not have transparency
    {
      s = pDraw->pPixels;
      // Translate the 8-bit pixels through the RGB565 palette (already byte reversed)

      for (x=0; x<pDraw->iWidth; x++)
      {
        uint16_t _rotatedX, _rotatedY;
        _rotatedX = x + g_gif_play_x_offset;
        _rotatedY = y + g_gif_play_y_offset;

        getRotationPos(&_rotatedX, &_rotatedY, g_rotationValue);
        ledpannel.drawPixelColorCheck(_rotatedX, _rotatedY, usPalette[*s++], OVER_WRITE_DRAW_FLAG);
      }
    }
} /* GIFDraw() */


void * GIFOpenFile(const char *fname, int32_t *pSize)
{
  // Serial.printf("GIFOpenFile \n");

  f = FILESYSTEM.open(fname);
  if (f)
  {
    *pSize = f.size();
    return (void *)&f;
  }
  return NULL;
} /* GIFOpenFile() */

void GIFCloseFile(void *pHandle)
{
  // Serial.printf("GIFCloseFile \n");

  File *f = static_cast<File *>(pHandle);
  if (f != NULL)
     f->close();
} /* GIFCloseFile() */

int32_t GIFReadFile(GIFFILE *pFile, uint8_t *pBuf, int32_t iLen)
{
    // Serial.printf("GIFReadFile \n");
    int32_t iBytesRead;
    iBytesRead = iLen;
        // Serial.printf("iBytesRead : %d \n", iBytesRead);

    File *f = static_cast<File *>(pFile->fHandle);
    // Note: If you read a file all the way to the last byte, seek() stops working
    if ((pFile->iSize - pFile->iPos) < iLen)
       iBytesRead = pFile->iSize - pFile->iPos - 1; // <-- ugly work-around
    if (iBytesRead <= 0)
       return 0;
    iBytesRead = (int32_t)f->read(pBuf, iBytesRead);
    pFile->iPos = f->position();
    return iBytesRead;
} /* GIFReadFile() */

int32_t GIFSeekFile(GIFFILE *pFile, int32_t iPosition)
{ 
  // Serial.printf("GIFSeekFile \n");

  int i = micros();
  File *f = static_cast<File *>(pFile->fHandle);
  f->seek(iPosition);
  pFile->iPos = (int32_t)f->position();
  i = micros() - i;
//  Serial.printf("Seek time = %d us\n", i);
  return pFile->iPos;
} /* GIFSeekFile() */

/**************************************************************************/
/** 
 * @brief   - 플래시에서 Gif 데이터 읽어와서 처리하는 함수
*/
/**************************************************************************/
void ShowGIF(char *name)
{
  if (gif.open(name, GIFOpenFile, GIFCloseFile, GIFReadFile, GIFSeekFile, GIFDraw))
  {
    Serial.flush();
    mIsGifPlay = true;

    xTaskCreatePinnedToCore (
      gifPlayFrame,                 // NeoPixel RGB LED를 표시하기 위한 태스크
      "gifPlayFrame",    // 태스크 이름
      // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
      4096, // 스택 할당 크기 (number of bytes)
      (void *) 0,
      1,                        // 태스크 우선 순위 
      &TaskGifCore1,                  // 태스크 핸들
      APP_CPU_NUM                         // 태스크가 실행될 코어 
    );
  }
} 
/* ShowGIF() */
/**************************************************************************/
/** 
 * @brief   - 페이지 버퍼에서 Gif 데이터 읽어와서 처리하는 함수
 * @details - 우리의 페이지 형식에 맞게 오버로딩
 *          - 배경 GIF 인 경우 DISPLAY_OPTION_IDX 비트맵 마스크 검사하여 추출
/**************************************************************************/
void ShowGIF(int pageCount)
{
  if ((pageBuffer[pageCount][DISPLAY_OPTION_IDX] & OPTION_GIF_BACKGROUND) != 0) { //  gif background 검사
    ShowGIFBackGround(pageCount);
    return;
  }

  if (mIsGifPlay) { //  gif task 끝난 시점 500millis 이상 지나고, mIsGifPlay 실행 중이지 않은 상태에만 gif 출력
    return;
  }
  delay(100); //  Task Core 생성 안정 주기 필수! 

  categoryActionFlag = true;
  nextPageToken = false;
  if (gif.open((uint8_t*)&pageBuffer[pageCount][SIZE_CMD + SIZE_OPTION + SIZE_PAGE + GIF_X_Y_OFFSET_SIZE], pageSize[pageCount], GIFDraw)) // SIZE_CMD + SIZE_OPTION is idx 0 == CMD, 1 == OPTION
  {
    Serial.flush();
    mIsGifPlay = true;

    g_gif_play_x_offset = (int) pageBuffer[pageCount][GIF_FLASH_X_OFFSET_IDX];
    g_gif_play_y_offset = (int) pageBuffer[pageCount][GIF_FLASH_Y_OFFSET_IDX];

    xTaskCreatePinnedToCore (
      gifPlayFrame,                 // NeoPixel RGB LED를 표시하기 위한 태스크
      "gifPlayFrame",    // 태스크 이름
      // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
      4096, // 스택 할당 크기 (number of bytes)
      (void *) GIF_END_NEXT_PAGE_OK_VALUE,
      1,                        // 태스크 우선 순위 
      &TaskGifCore1,                  // 태스크 핸들
      APP_CPU_NUM                         // 태스크가 실행될 코어 
    );
  }
} /* ShowGIF() */

/* ShowGIFBackGround() */
/**************************************************************************/
/** 
 * @brief   - 배경 Page Buffer에서 처리하는 함수 (DisPlayMode에서 재생 - 페이지 관련 Token과 분기 처리)
 * @details - Page Token 관계 없이 출력
/**************************************************************************/
void ShowGIFBackGround(int pageCount)
{
  if (mIsGifPlay)   //  gif task 끝난 시점 500millis 이상 지나고, mIsGifPlay 실행 중이지 않은 상태에만 gif 출력
    return;

  if (categoryActionFlag) { //  해당 페이지 컨텐츠 액션이 끝났다면 Gif 실행하지 않음
    if (nextPageToken) {
      // Serial.printf("next page token!! \n");
      return;
    }
  } else {
    unsigned long _temp_pre_time = page_interval_pre_time;  //  원본 값 복사되지 않게 temp 변수 생성
    if (delayExt(millis(), &_temp_pre_time, displayPageTime)) { //  해당 페이지 컨텐츠 출력 시간이 끝났다면 Gif 실행하지 않음
      // Serial.printf("delayExt token!! \n");
      return;
    }
  }

  delay(100);  //  Task Core 생성 안정 주기 필수!

  if (gif.open((uint8_t*)&gif_background_page_buffer[pageCount][SIZE_CMD + SIZE_OPTION + SIZE_PAGE + GIF_X_Y_OFFSET_SIZE], gif_background_page_size[pageCount], GIFDraw)) // SIZE_CMD + SIZE_OPTION is idx 0 == CMD, 1 == OPTION
  {
    Serial.flush();
    mIsGifPlay = true;

    g_gif_play_x_offset = (int) gif_background_page_buffer[pageCount][GIF_FLASH_X_OFFSET_IDX];
    g_gif_play_y_offset = (int) gif_background_page_buffer[pageCount][GIF_FLASH_Y_OFFSET_IDX];

    xTaskCreatePinnedToCore (
      gifPlayFrame,                 // NeoPixel RGB LED를 표시하기 위한 태스크
      "gifPlayFrame",    // 태스크 이름
      // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
      4096, // 스택 할당 크기 (number of bytes)
      (void *) 0,
      1,                        // 태스크 우선 순위 
      &TaskGifCore1,            // 태스크 핸들
      APP_CPU_NUM               // 태스크가 실행될 코어 
    );
  }
} /* ShowGIF() */


/* ShowGIF() */
/**************************************************************************/
/** 
 * @brief   - SRAM에 저장되어있는 Gif 데이터로 처리하는 함수 
 * @details - 우리의 페이지 형식에 맞게 오버로딩
 * @param _gif_data - gif 파일 바이너리 데이터
 * @param _data_count - gif 파일 데이터 길이
/**************************************************************************/
void ShowGIF(uint8_t* _gif_data, int _data_count)
{
  if (mIsGifPlay) { //  gif task 끝난 시점 500millis 이상 지나고, mIsGifPlay 실행 중이지 않은 상태에만 gif 출력
    return;
  } 
  delay(100); //  Task Core 생성 안정 주기 필수! 
   
  if (gif.open(_gif_data, _data_count, GIFDraw)) 
  {
    Serial.flush();
    mIsGifPlay = true;
    task_delay_tick = 0;

    xTaskCreatePinnedToCore (
      gifPlayFrame,                 // NeoPixel RGB LED를 표시하기 위한 태스크
      "gifPlayFrame",    // 태스크 이름
      // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
      4096, // 스택 할당 크기 (number of bytes)
      (void *) 0,
      1,                        // 태스크 우선 순위 
      &TaskGifCore1,            // 태스크 핸들
      APP_CPU_NUM               // 태스크가 실행될 코어 
    );
  } 
} /* ShowGIF() */

/**
 * @brief Play Frame
 * 
 * @param _isNextPage - gif 재생 끝난 후 다음 페이지 변환 할지 bool value -> 0 == next page non, 1 == next page ok (gif 배경 일 경우 다음 페이지로 전환 하지 않음) 
 */
void gifPlayFrame(void *param) {
  while (gif.playFrame(true, NULL) && ledpannel.m_gif_action_Flag)
  {
    vTaskDelay(5);
  }
  gif.close();

  if ((int) param == GIF_END_NEXT_PAGE_OK_VALUE) {  //  다음 페이지로 전환  (Page Gif)
    nextPageToken = true;
    ledpannel.m_gif_action_Flag = false;
    // Serial.printf("param == GIF_END_NEXT_PAGE_OK_VALUE \n");
  } else {  //  다음 페이지로 전환하지 않음 (Back ground)
    // Serial.printf("param != GIF_END_NEXT_PAGE_OK_VALUE \n");
  }

  task_delay_tick = millis();
  mIsGifPlay = false;
  TaskGifCore1 = NULL;
  vTaskDelete(NULL);
}