#ifndef _FLASH_
#define _FLASH_

#include <FS.h>
#include <LittleFS.h>

/*  File Name  */
#define FIREBASE_REQ_KEY    "/server_req_key.txt"   // 파이어 베이스 요청 Url 키

/* File Result  Code  */
#define FILE_NOT_EXIST  -1

/*  페이지에 내용 존재하는지 저장 LIST  */
#define FLASH_PAGE_ENABLE_LIST  "/page_enable_list"

/*  페이지 Enable Vaule */
#define FLASH_PAGE_ENABLE_VALUE 1
#define FLASH_PAGE_DISENABLE_VALUE 0

void deleteFile(fs::FS &fs, const char * path);
void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void writeFile(fs::FS &fs, const char * path, uint8_t* buffer, size_t size);
int readFile(fs::FS &fs, const char * path, uint8_t * buffer);
void flashFormat();
void allPageDelete();

#endif