#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>
#include <HTTPClient.h>
#include <ESP32httpUpdate.h>
#include <Arduino.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include "../communication/ota.h"
#include "../board/config.h"
#include "../utils/jsonParse.h"
#include "../utils/etc.h"
#include "../utils/localtime.h"

String updateInfoUrl;

/*  시간 Sync Flag  */
bool isTimeSynch = false;

const char* host = "esp32";

/*   http.get 에 대한 payload 값 가져옴. (해당 url에 대한 데이터를 담기위한 변수) */
String payload; 
String versionNum ="";
String updateUrl = "";

extern int int_flg;  

bool wifi_open(char* ssid, char* password){
  int statue_check_count = 0;
  unsigned long cur_time = 0, pre_time = 0; 

  if  (WiFi.status() != WL_CONNECTED){
    WiFi.begin(ssid, password);

    while (1) { //  Loop for connection
      cur_time = millis();             //  페이지 내용이 Default, Blink 라면 5초 후에 다음페이지 전환 
      if (delayExt(cur_time, &pre_time, 2500)) {
        if (WiFi.status() == WL_CONNECTED) break;
        else if(statue_check_count > 7 || int_flg == 1){
          Serial.println("Wifi is Disconnect..");
          wifi_close();
          return false;
        }
        pre_time = cur_time;
        statue_check_count++;
      }
    }
  } 
  else {
    Serial.println("Aleady Wifi is Opened");
  }
  isTimeSynch = true;
  return true;
}
void wifi_close(){
  WiFi.disconnect();
}

int get_updateUrl(){
   if ((WiFi.status() == WL_CONNECTED)) { //Check the current connection status
   
    #if BOARD_TYPE == MASTER
      updateInfoUrl = "https://tistory1.daumcdn.net/tistory/4181558/skin/images/updateVerInfo.txt";
    #elif BOARD_TYPE == YOUFUNI
      updateInfoUrl = "https://tistory1.daumcdn.net/tistory/4191138/skin/images/updateVerInfo.txt";
    #endif
 
    HTTPClient http;
 
    http.begin(updateInfoUrl); //  업데이트 바이너리 파일 Url Path 기록 되어있는 위치
    int httpCode = http.GET();                                                  //Make the request
    Serial.println(httpCode); //  return to httpCode 
    if (httpCode > 0) { //  Check for the returning code
        payload = http.getString();
        Serial.println(payload);  //  paylad : update url   

        versionNum = jsonParseWordStr(payload, "VER");
        updateUrl = jsonParseWordStr(payload, "URL");
        http.end(); //Free the resources

        if (versionNum == FIRM_VERSION ) {
          // Serial.println("버전 같음");
          return VERSION_ERROR_CODE;
        } else if (versionNum < FIRM_VERSION) {
          // Serial.println("이전 버전");
          return VERSION_ERROR_CODE;
        } else {
          // Serial.println("최신 버전");
          return VERSION_SUCCESS_CODE;
        }
    }
    else {
      Serial.println("Error on HTTP request");
      http.end(); //Free the resources
      return WIFI_DISCONNECT;
    }
  }
  return -99;
}

int update_urlVesrion(){
   if((WiFi.status() == WL_CONNECTED)) {

        t_httpUpdate_return ret = ESPhttpUpdate.update(updateUrl);

        switch(ret) {
            case HTTP_UPDATE_FAILED:
                Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
                Serial.println();
                break;

            case HTTP_UPDATE_NO_UPDATES:
                Serial.println("HTTP_UPDATE_NO_UPDATES");
                break;

            case HTTP_UPDATE_OK:
                Serial.println("HTTP_UPDATE_OK");
                return HTTP_UPDATE_OK;
                break;
        }
    } else {
      return WIFI_DISCONNECT;
    }
    return 0;
}
String get_localIP() {
  String ip = WiFi.localIP().toString().c_str();
  return ip;
}

bool getWifiStatus() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

String wifiScan(void) {
  // WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  int n = WiFi.scanNetworks();
  String wifiScanList = "";

  if (n == 0) {
      Serial.println("no networks found");
  } else {
      // Serial.print(n);
      // Serial.println(" networks found");
      for (int i = 0; i < n; ++i) {
          // Print SSID and RSSI for each network found
          // Serial.print(i + 1);
          // Serial.print(": ");
          // Serial.println(WiFi.SSID(i));
          wifiScanList.concat(",");
          wifiScanList.concat(WiFi.SSID(i));
          wifiScanList.concat(",");
          wifiScanList.concat(WiFi.RSSI(i));
          // Serial.printf("wifiScanList : %s \n", wifiScanList);
          // Serial.printf("wifiScanList.length() : %d \n", wifiScanList.length());
      }
  }
  WiFi.disconnect();
  return wifiScanList;
}
