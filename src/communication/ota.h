#ifndef _OTA_
#define _OTA_
// Version Check Define
#define VERSION_ERROR_CODE  0
#define VERSION_SUCCESS_CODE  1
#define URL_CONNECT_FAIL_CODE  -1

// Update Process Check Define
#define WIFI_DISCONNECT  -2

#define ETH_ADDR        1
#define ETH_POWER_PIN   5
#define ETH_MDC_PIN     23
#define ETH_MDIO_PIN    18
#define ETH_TYPE        ETH_PHY_IP101

void server_run();
void wifi_server_open();
bool wifi_open(char* ssid, char* password);
void wifi_close();
int get_updateUrl();
int update_urlVesrion();
String get_localIP();
bool getWifiStatus();
String wifiScan();

#endif
