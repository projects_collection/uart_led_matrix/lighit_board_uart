/**
 * @file lighit_board.ino
 * @author your name (you@domain.com)
 * @brief IDE Setting Read Me 참조 
 * @version 0.3.1
 * @date 2022-06-22
 * @copyright frontier (c) 2022
 * 
 */

#include <Adafruit_GFX.h>   // Core graphics library
#include <P3RGB64x32MatrixPanel.h>
#include <BluetoothSerial.h>
#include <FS.h>
#include <LittleFS.h>
#include <Preferences.h>
#include <BLEDevice.h>
#include <ESP32-HUB75-MatrixPanel-I2S-DMA.h>
#include <SPI.h>
#include <LoRa.h>
#include "popsign.h"
#include "src/communication/ota.h"
#include "src/communication/firebase.h"
#include "src/communication/bluetooth.h"
#include "src/communication/buffer_serial.h"
#include "src/board/board.h"
#include "src/board/config.h"
#include "src/flash/flash.h"
#include "src/led_matrix/ledpannel.h"
#include "src/led_matrix/display_page.h"
#include "src/utils/etc.h"
#include "src/utils/localtime.h"
#include "src/utils/jsonParse.h"
#include "src/img/bitmap_mode_logo.h"
#include "src/utils/tts.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

/**
 * @brief Board Config Info 
 * @details member - model_info (보드 모델), matrix_width (매트릭스 넓이),
 *                   matrix_height(매트릭스 높이), flash_size (플래시 크기);
 */
Board_Config g_board_config(MODEL_INFO, IDE_MATRIX_WIDTH, IDE_MATRIX_HEIGHT, FLASH_MOMORY_SIZE);  //  Arudino IDE에서 설정한 내용 Defalut로 삽입.

/** 
 * @brief   Matrix - LED Pin Draw Matrix 
 *          * 모델별로 객체 달리 가져갈 수 있는 typedef 변수
 *          * P3RGB64x32MatrixPanel or GlassLed 객체
*/
MATRIX *matrix;

/** 
 * @brief   - 매트릭스 , 비트맵 관련 출력 처리 객체
*/
LedPannel ledpannel;
/** 
 * @brief   - 블루투스 통신 객체
*/
BluetoothSerial SerialBT;
/** 
 * @brief   - 블루투스 통신 패킷 프로토콜 처리 객체
*/
BufferSerial bufferSerial = BufferSerial(&SerialBT);

/** 
 * @brief   - 블루투스 함수 차일드 객체
*/
Bluetooth bluetooth(&SerialBT);

/* delay 값 관련 변수 */
unsigned long pre_time = 0; 

/* 폰트 관련 변수 */
char **strArr = NULL;
int lineCnt = 1;
int16_t font_total_width = 0, font_total_height = 0;

/*  Mode 관련 변수 */
int modeStep = 0;
int int_flg = 0;                //  Mode 변경 Flag
int resource_release_flag = 0;  //  Wifi 자원 해제 Flag

uint8_t modeNum = MODE_BLUETOOTH;
uint8_t startModeNum = MODE_BLUETOOTH;
float ledDivLevel = 1;
uint8_t useModeList[MODE_COUNT] = {MODE_BLUETOOTH}; 
uint8_t modeCount = 1;
// 로고 시간 
int logoTime = 2000;
// 로고 이용 
uint8_t logoEnable = LOGO_ENABLE_BIT_MASK_DEFAULT;

/*  현재 로테이션 값  */
uint8_t g_rotationValue = ROTATION_VALUE_LANDSCAPE;
int g_rotation_start_pos = 0; //  로테이션에 따른 x 좌표 위치 (세로 모드 일 경우 시작 좌표 변경 됨)

/*  디스플레이 모드 관련 변수  */
size_t pageSize[PAGE_TOTAL_COUNT] = {0};

int8_t g_pageCount = 0;
bool nextPageToken = false;
bool categoryActionFlag = false;
int displayPageTime = 5000;
// 페이지 전환 모드 (수동 or 자동) 
bool autoNextPageMode = true;
// 페이지 버퍼
char* pageBuffer[PAGE_TOTAL_COUNT] = {NULL, };
// 페이지 Enable List --> 페이지 사용가능하는지 가지고 있는 버퍼 (모든 페이지 접근 하는데에 시간이 오래걸려 해당 버퍼 사용)
uint8_t pageEnableList[PAGE_TOTAL_COUNT] = {0};
bool isSinglePagePrint = true;  // 단일 페이지일 경우 깜빡거리지 않게 하기 위해 설정
uint8_t display_step = 0; //  디스플레이 모드 출력 스텝
//  Board 정보 배터리 Or 상시 전원
int8_t isBatteryModeInfo = POPSIGN_NON_BATTERY_MODE;

/* Logo */
bool isCustomLogo = false;
bool isBlinkBTLogo = true;  //  블루투스 bitmap 점멸할지 Check 변수

/* Preferences Flash */
Preferences prefs;  
uint8_t prefsBoardInfoArray[BOARD_INFO_PREFS_IDX_TOTAL_COUNT] = {0};

// 블루투스 이름 
String bt_name = "PopSign";

/*  PAGE TIME SWICH INFO   */
struct tm g_timeinfo;

/*  구동 시간대 설정 관련 변수 */
bool timeSwitchIsEnable[PAGE_TOTAL_COUNT] = {false};
int8_t timeSwitchStartHour[PAGE_TOTAL_COUNT] = {0}, timeSwitchStartMinute[PAGE_TOTAL_COUNT] = {0};
int8_t timeSwitchFromHour[PAGE_TOTAL_COUNT] = {0}, timeSwitchFromMinute[PAGE_TOTAL_COUNT] = {0};
extern bool isTimeSynch;

/*  Server FireBase   */
FirebaseData* firebaseData = NULL;
String streamDataString = "";
String serverRequestUrl = "";
bool isStreamCallback = false;

/* GIF 출력 관련 변수 */
AnimatedGIF gif;

/* TTS 관련 변수 */
uint8_t* tts_data = NULL;
// uint8_t g_sensor_page_count = 0;
uint8_t* tts_wav_page_buffer[PAGE_TOTAL_COUNT] = {NULL, };
// GIF 배경 Data Buffer
uint8_t* gif_background_page_buffer[PAGE_TOTAL_COUNT] = {NULL, };
// GIF 배경 Data Size
size_t gif_background_page_size[PAGE_TOTAL_COUNT] = {0};

/*  Bitmap Image 관련 변수 */
bool g_logo_blink_flag = true;
uint16_t g_copy_bitmap[7*11] = {1};

/*  초기화 및 버전 정보 Flag */
int8_t g_initModeStep = LOOP_MODE_EXCUTE;
bool preventContinuousClicks = false; //  모드 연속 변경 막기위해 Flag 생성 
bool isContinueClickedBtn = false;  //  모드 전환 버튼 지속해서 클릭되고 있는지 체크 변수
unsigned long pre_time_reset = 0; 

//  무게 인식 최소 값
int g_weight_recognitition = 300000;
//  Lora Sub mode (페이지 출력, 게이지 출력)
int g_lora_sub_mode_type = LORA_CONTENT_PAGE_PRINT_MODE;
//  Gage Mode 관련 변수 - 게이지 갯수
int g_gage_count = 10;
//  기본 페이지 출력 중인 경우 
bool isDefalutIagePrint = true;  
//  페이지 출력 간격 유지 시간
int displayPageIntervalTime = DEFALUT_PAGE_INTERVAL_TIME; 

/*  Multi Core Task_0 */
TaskHandle_t Task_0;

void setup() {
  //Serial Begin
  Serial.begin(115200);

  //Flash Begin
  if(!LittleFS.begin(true)){
      Serial.println("LittleFS Mount Failed");
  }

  // Module configuration
  HUB75_I2S_CFG mxconfig(
    g_board_config.matrix_width,   // module width
    g_board_config.matrix_height,   // module height
    PANEL_CHAIN    // Chain length
  );

  /**
   * @brief 모델 정보에 따른 Pin 및 설정 
   * 
   */
  #if MODEL_VALUE == MASTER_0064_0032_16M
    Serial.printf("Model : %lld \n", MASTER_0064_0032_16M);
    mxconfig.min_refresh_rate = 150;
  #elif MODEL_VALUE == MASTER_0128_0032_16M
    Serial.printf("Model : %lld \n", MASTER_0128_0064_16M);
    mxconfig.gpio.r1 = 14; 
    mxconfig.min_refresh_rate = 150;
  #elif MODEL_VALUE == MASTER_0192_0032_16M
    Serial.printf("Model : %lld \n", MASTER_0192_0032_16M);
    mxconfig.gpio.r1 = 14;
    mxconfig.min_refresh_rate = 250;
  #elif MODEL_VALUE == VOICE_MODEL_0064_0032_16M
    Serial.printf("Model : %lld \n", VOICE_MODEL_0064_0032_16M);
    mxconfig.gpio.r1 = 14;
  #elif MODEL_VALUE == LORA_0192_0032_16M
    Serial.printf("Model : %lld \n", LORA_0192_0032_16M);
    mxconfig.gpio.r1 = 14;
    mxconfig.min_refresh_rate = 250;
  #elif MODEL_VALUE == YOUFUNI_0064_0032_16M
    Serial.printf("Model : %lld \n", YOUFUNI_0064_0032_16M);
    mxconfig.gpio.r1 = 14; 
    mxconfig.min_refresh_rate = 150;
  #else
    Serial.printf("Board is not selected!\n");
  #endif

  mxconfig.clkphase = false;
  mxconfig.driver = HUB75_I2S_CFG::FM6124;

  // Display Setup
  matrix = new MatrixPanel_I2S_DMA(mxconfig);
  matrix->begin();
  matrix->setLatBlanking(1); //0-255
  matrix->setBrightness8(255); //0-255
  matrix->clearScreen();

  ledpannel.setup(matrix, &bufferSerial);
  isCustomLogo = read_nvs_board_info();  // 로고 저장되어 있으면 먼저 출력

  if (!isCustomLogo)  device_init_start();

  int8_t modePin = MODE_PIN;
  bool checkUpdate = false;
  pinMode(modePin, INPUT_PULLUP);       //  모드 전환 핀 
  pinMode(BATTERY_PIN, INPUT_PULLDOWN); //  배터리 핀
  pinMode(SENSOR_PIN, INPUT);           //  센서 핀 2 
  pinMode(WAKEUP_PIN, INPUT);           //  WAKEUP 핀 2 

  /*  배터리 전원 모델 or 상시 전원 모델 체크 */
  int adcValue = analogRead(BATTERY_PIN) * REGISTER_DIST; 
  if (adcValue >= 2000) isBatteryModeInfo = POPSIGN_BATTERY_MODE;
  else isBatteryModeInfo = POPSIGN_NON_BATTERY_MODE;

  Serial.printf("Pop Sign Version : %s \n", FIRM_VERSION);
  //  플래시에 저장 되어있는 세팅 정보 리드 (모드 정보, LED 밝기, 업데이트 유무, 매트릭스 크기, 로고 시간, 디스플레이 설정 시간 등)
  getSettingInfo(&modeNum, &ledDivLevel, &modeCount, useModeList, sizeof(useModeList), &checkUpdate, &startModeNum, &logoTime, &displayPageTime);
  //  매트릭스 크기에 따른 Rotation 값 설정
  ledpannel.setupRotation();
  ledpannel.setupFont(1);
  if (isBatteryModeInfo == POPSIGN_BATTERY_MODE)  Serial.println("Battery Power Model!");
  else Serial.println("Permanent Power Model!");
  if (checkUpdate == true)  ledpannel.print_str_matrix(1, "업데이트\n성공"); //  Config 버전 정보 달라졌을 경우

  Serial.printf("modeCount : %d \n", modeCount);

  /*  보드 타입에 따른 사용 모드 설정 (BT MODEL or UART MODEL) */
  if (g_board_config.model_info == YOUFUNI || g_board_config.model_info == ENT) {
    Serial.printf("uart!! \n");
    Serial2.begin(115200, SERIAL_8N1, RX, TX);
    useModeList[0] = MODE_UART;
    useModeList[1] = MODE_BLUETOOTH;
    useModeList[2] = MODE_DISPLAY;
    modeCount = 3;
    modeNum = 0;
  }
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_DEFAULT, ESP_PWR_LVL_P9);
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_ADV, ESP_PWR_LVL_P9);
  esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_SCAN ,ESP_PWR_LVL_P9);
  disableCore0WDT();

  // xTaskCreatePinnedToCore (
  //   mode_excute,                 // NeoPixel RGB LED를 표시하기 위한 태스크
  //   "mode_task",    // 태스크 이름
  //   // configMINIMAL_STACK_SIZE, // 스택 할당 크기 (number of bytes)
  //   10000, // 스택 할당 크기 (number of bytes)
  //   NULL,                     // 태스크 인수
  //   1,                        // 태스크 우선 순위 
  //   &Task_0,                  // 태스크 핸들
  //   0                         // 태스크가 실행될 코어 
  // );
  
  // matrix->stopDMAoutput();
  gif.begin(LITTLE_ENDIAN_PIXELS);
  log_ps_memory();
  attachInterrupt(digitalPinToInterrupt(modePin), mode_change_event, HIGH);  // ModeChange를 위한 이벤트 인터럽트 설정 (Mode Pin - GPIO13)
  esp_bredr_tx_power_set(ESP_PWR_LVL_P9,ESP_PWR_LVL_P9);  // Bluetooth Tx Power Level 설정 }
}

void loop()
{
    check_flash_reset();  //  리셋, 버전 출력, 수동 모드 탈출 Check  
    mode_change_check();  //  모드 전환 Check
    
    if (g_initModeStep != LOOP_VERSION_PRINT) {     //  버전 정보 출력 모드일 경우 하위 모드 출력하지 않음
      if (getModeNum(modeNum) == MODE_BLUETOOTH)    bluetoothMode();
      else if (getModeNum(modeNum) == MODE_DISPLAY) displayMode();
      else if (getModeNum(modeNum) == MODE_UART) uartMode();
    } 
}

portTickType delay_10 = 1 / portTICK_RATE_MS;

void mode_excute(void *param)
{
  // Serial.print("neoPixel()이 실행되고 있는 코어: ");
  // Serial.println(xPortGetCoreID());
  while(true) {

    vTaskDelay(5);
  }
}

/**************************************************************************/
/** 
 * @fn    - bluetoothMode()
 * @brief - 블루투스 통신 및 수신 받은 패킷에 따른 LED 점멸
*/
/**************************************************************************/
void bluetoothMode() // 0 ~ 1 Logo Setion, 2 ~ 3 bluetooth init, 4 is bt ongoing
{
  if (modeStep == 0) {
    Serial.println("BLUETOOTH MODE!");
    bluetooth.setup();
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);
    uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
    if (!isCustomLogo) {
      matrix->drawRGBBitmap(x_pos_offset, 0, (uint16_t*) bluetooth_mode_logo_64_32, 64, 32);
      char str[3];
      uint8_t mac5[6];
      esp_read_mac(mac5, ESP_MAC_BT);
      matrix->setFont(NULL);
      sprintf(str, "%02x", mac5[5]);
      String strBtmac = str;
      matrix->setCursor(33 + x_pos_offset,5);
      matrix->setTextColor(matrix->color565(200, 200, 200));
      matrix->print(":" + strBtmac);
    }     
    pre_time = millis();
    modeStep = 1;
    matrix->copyRGBBitmapRect(45 + x_pos_offset, 17, 7, 11, g_copy_bitmap);
  } 
  else if (modeStep == 1) 
  {
    ledpannel.command_excute();
    if (!isCustomLogo) {
      if (delayExt(millis(), &pre_time, 1000) && isBlinkBTLogo) {
        // Serial.printf("isTimeSynch : %d \n", isTimeSynch);
        uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
        if (g_logo_blink_flag) {
          matrix->fillRect(45 + x_pos_offset, 17, 7, 11, 0);  //  해당 로고 지움
          g_logo_blink_flag = !g_logo_blink_flag;
        } else {
          int idx = 0;
          for (int j = 0; j < 11; j++) {
            for (int i = 0; i < 7; i++ ){
              matrix->drawPixel(i + 45 + x_pos_offset, j+17, g_copy_bitmap[idx++]); //  해당 로고 표시
            }
          }
          g_logo_blink_flag = !g_logo_blink_flag;
        }
      }
    }
    if(bluetooth.check())
    {
      // Serial.println("bluetooth OK!");
      modeStep = 2;
      isBlinkBTLogo = false;
    }  
  } 
  else if (modeStep == 2) {
    if (!SerialBT.hasClient()) {
      modeStep = 1;
      bluetooth.disconnect();
      delay(200);
      bluetooth.setup();
    }
    bufferSerial.process(matrix, &ledpannel);
    ledpannel.command_excute();
  } 
}

/**************************************************************************/
/** 
 * @fn    - displayMode()
 * @brief - 플래시에 저장된 페이지 출력
*/
/**************************************************************************/
void displayMode()
{
  if(bluetooth.check())
  {
    PRINTLN("블루투스 연결 시도 들어옴");
    bufferSerial.process(matrix, &ledpannel);
    return;
  } 
  /**
   * modeStep 0 : 디스플레이 모드 로고 출력
   * modeStep 1 : 디스플레이 모드 로고 시간 대기
   * modeStep 2 : 디스플레이 모드 페이지 출력
  */
  if (modeStep == 0)
  {
    Serial.println("DISPLAY MODE!");
    bluetooth.setup();
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);

    if (!isCustomLogo) {
      uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
      matrix->drawRGBBitmap(x_pos_offset, 0, (uint16_t*) display_mode_logo_64_32, 64, 32);
    } 
    pre_time = millis();
    modeStep = 1;
  }
  else if (modeStep == 1) 
  {
    if (delayExt(millis(), &pre_time, logoTime)) {  //  로고 LogoTime 만큼 출력
    } else return;
    modeStep = 2;
    display_step = 0;
    pre_time = 0;
    ledpannel.matrixClear();   //  로고 출력 후 화면 지움
  }
  else if (modeStep == 2) 
  {
    play_display(matrix, &ledpannel);
  }
}

void loraMode()
{
  if(bluetooth.check())
  {
    Serial.printf("블루투스 연결 시도 들어옴\n");
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);
    bufferSerial.process(matrix, &ledpannel);
    modeStep = 3;
    return;
  } 
  /**
   * modeStep 0 : Logo Pirnt
   * modeStep 1 : Lora Mode SetUp
   * modeStep 2 : Lora Packet Process
  */
   if (modeStep == 0)
  {
    Serial.println("loraMode MODE!");
    ledpannel.print_str_matrix_basic(1, "Lora", 0, matrix->color565(0,255,0));
    pre_time = millis();
    modeStep = 1;
  } 
  else if (modeStep == 1) {
    if (delayExt(millis(), &pre_time, logoTime)) {  //  로고 LogoTime 만큼 출력
      ledpannel.multiPagePrint(LORA_DEFALUT_PAGE); 
    } else return;
    bluetooth.setup();
    delay(300);
    SPI.begin(SCK_PIN, MISO_PIN, MOSI_PIN, SS_PIN);
    LoRa.setPins(SS_PIN, RST_PIN, DIO0_PIN);

    long lora_frequency;  //  주파수
    int lora_sync_word;   //  Sync Word

    //  모드 타입에 따라 주파 수 및 Sync Word Init
    if (g_lora_sub_mode_type == LORA_CONTENT_PAGE_PRINT_MODE)  //  페이지 출력 모드 조건
    {
      lora_frequency = 866E6;
      lora_sync_word = 0xF7;
      Serial.printf("lora F3\n");
    }
    else if (g_lora_sub_mode_type == LORA_CONTENT_GAGE_PRINT_MODE) //  게이지 출력 모드 조건
    {
      lora_frequency = 915E6;
      lora_sync_word = 0x3A;
      Serial.printf("lora 3A\n");
    }
    else // Defalut
    {
      lora_frequency = 866E6;
      lora_sync_word = 0xF7;
      Serial.printf("lora F3\n");
    }

    while (!LoRa.begin(lora_frequency)) { //  Lora Begin
      Serial.println(".");
      delay(500);
    }

    LoRa.setSyncWord(lora_sync_word); //  Sync Wort Set

    // Change sync word (0xF3) to match the receiver
    // The sync word assures you don't get LoRa messages from other LoRa transceivers
    // ranges from 0-0xFF
    //LoRa.setSyncWord(0x3A);
    //LoRa.setSyncWord(0xF3);

    Serial.println("LoRa Initializing OK!");
    bufferSerial.set_serial_type(TRANSMIT_LORA);
    
    byte sendBuffer[4] = {0};
    sendBuffer[0] = ((g_weight_recognitition & 0xff000000) >> 24);
    sendBuffer[1] = ((g_weight_recognitition & 0x00ff0000) >> 16);
    sendBuffer[2] = ((g_weight_recognitition & 0x0000ff00) >> 8);
    sendBuffer[3] = (g_weight_recognitition & 0x000000ff);
    bufferSerial.transmit_data(SET_WEIGHT_RECOGNITION_MIN_VALUE, (char*) sendBuffer, 4);
    Serial.printf("g_weight_recognitition : %d \n", g_weight_recognitition);

    modeStep = 2;
  } 
  else if (modeStep == 2) 
  {
    bufferSerial.process(matrix, &ledpannel);
    ledpannel.command_excute();
    if (!isDefalutIagePrint) {
      if (checkNextPageCondition(displayPageTime, &ledpannel)) {  // 페이지 출력 중단 검사
          ledpannel.set_action_off();
          ledpannel.multiPagePrint(LORA_DEFALUT_PAGE); 
          isDefalutIagePrint = true;
          categoryActionFlag = false;
          nextPageToken = true;
        if (pageBuffer[g_pageCount] != NULL) { 
          free(pageBuffer[g_pageCount]);
          pageBuffer[g_pageCount] = NULL;
        }
      } else return;
    }
  } 
  else if (modeStep == 3)   //  블루투스 연결 시도 처리
  {
    bufferSerial.process(matrix, &ledpannel);

    if (!SerialBT.hasClient()) {
      modeStep = 2;
      bluetooth.disconnect();
      delay(200);
      bluetooth.setup();
      bufferSerial.set_serial_type(TRANSMIT_LORA);
      ledpannel.set_action_off();
      ledpannel.multiPagePrint(LORA_DEFALUT_PAGE); 
    }
  } 
}

/**************************************************************************/
/** 
 * @fn    - wifiMode()
 * @brief - 와이파이 통신 (파이어베이스)
*/
/**************************************************************************/
void wifiMode()
{
  /** 
   * modeStep 0 : 로고 출력
  */
  if (modeStep == 0)
  {
    Serial.println("WiFi MODE!");
    if (!isCustomLogo) {
      uint8_t x_pos_offset = (g_board_config.matrix_width - 64) / 2;
      matrix->drawRGBBitmap(x_pos_offset, 0, (uint16_t*) wifi_mode_logo_64_32, 64, 32);
    } 
    
    pre_time = millis();
    modeStep = 1;
  } 
  /** 
   * modeStep 1 : 인터넷 연결
  */
  else if(modeStep == 1) {
    if (delayExt(millis(), &pre_time, logoTime)) {
              fillScreenEx(0, 0, 0, matrix);
    } else {
      return;
    }
      char ssidDatas[20];
      char passwordDatas[20];
      char* buffer = NULL;
      int bufSize;
      int j = 0;
      String str;
      String strTemp;
      File file;
      //  페이지 내용이 Default, Blink 라면 5초 후에 다음페이지 전환 

      // ledpannel.set_matrix_end();
      bufferSerial.set_serial_type(TRANSMIT_WIFI_PACKET);
      
      if (!getWifiStatus()) {
        // Serial.println("와이파이 연결 안 되어 있음!");
        file = LittleFS.open("/wifiInfo.txt", "r");
        bufSize = file.size();
        if (bufSize == 0) {
          ledpannel.print_str_matrix_basic(1,"Need the\n"
                                      "Wi-Fi Info", 0, matrix->color565(255,0,0));
          return;
        } 
        buffer = (char*)ps_malloc(bufSize);
        // Serial.printf("bufSize : %d \n", bufSize);
        
        while (file.available()) {
          buffer[j++] = file.read();
        }

        str = buffer;
        file.close();
        free(buffer);

        /*  Wifi ID 값 추출 */
        strTemp = jsonParseWordStr(str, "ID");
        strcpy(ssidDatas, strTemp.c_str());

        /*  Wifi PW 값 추출 */
        strTemp = jsonParseWordStr(str, "PW");
        strcpy(passwordDatas, strTemp.c_str());
        
        ledpannel.print_str_matrix_basic(1,"wait", 0, matrix->color565(255,0,0));
        if (wifi_open(ssidDatas, passwordDatas)) {  // This Case : Wifi Connected Success
          matrix->clearScreen();
          ledpannel.print_str_matrix(1, "Wifi Ok");
          modeStep = 2;
        } else { // This Case : Wifi Connected Fail
          if (int_flg == MODE_CHANGE) return;
          critical_section_begin();
          // Serial.println("Wifi Fail!");
          ledpannel.print_str_matrix_basic(1,"Wifi Fail!", 0, matrix->color565(255,0,0));
          delay(2000);
          modeStep = 0;
          critical_section_end();
          pre_time = 0;
          return;
        }
      } else {  //  와이파이가 이미 연결되어 있는 경우
        matrix->clearScreen();
        ledpannel.print_str_matrix(1, "Wifi Ok");
        modeStep = 2;
      } 
  }
  /**
   * modeStep 2 : firebaseData 객체 생성 및 요청 Url 플래시 리드
  */
  else if(modeStep == 2) {    
    firebaseData = new FirebaseData();
    Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
    Firebase.reconnectWiFi(true);
    isTimeSynch = true;

    File file;
    file = LittleFS.open(FIREBASE_REQ_KEY);
    int fileSize = file.size();
    
    if (fileSize == 0) {
      char mac_str[13] = {0};
      uint8_t mac5[6];
      esp_read_mac(mac5, ESP_MAC_BT);
      sprintf(mac_str, "%x%x%x%x%x%x", mac5[0], mac5[1], mac5[2], mac5[3], mac5[4], mac5[5]);

      String strBtmac = "/";
      strBtmac.concat(mac_str);
      serverRequestUrl = strBtmac;
    } else {
      char* buffer = NULL;
      buffer = (char*) ps_malloc (fileSize + 1);
      fileSize = readFile(LittleFS, FIREBASE_REQ_KEY, (uint8_t *)buffer);
      buffer[fileSize] = '\0';

      serverRequestUrl = String(buffer);
      file.close();
      free(buffer);
    }
    modeStep = 3;
  }
  /**
   * modeStep 3 : 스트림 콜백 등록
  */
  else if(modeStep == 3) { 
    if (!Firebase.beginStream(*firebaseData, serverRequestUrl))
    {
      //  실패 시 return
      Serial.println("REASON: " + firebaseData->errorReason());
      delay(1000);
      return;
    }
    Firebase.setStreamCallback(*firebaseData, streamCallback, streamTimeoutCallback);
    time_gap_setup(SERVER_TIME_GAP_KOREA);
    get_localtime();    
    modeStep = 4;
    display_step = 0;
  }
  /**
   * modeStep 4 : 디스플레이 모드 Play
  */
  else if(modeStep == 4) { 
    play_display(matrix, &ledpannel);
  }
}

/** 
 * @fn    - mode_change_event()
 * @brief - 모드 변경 이벤트
**/
void IRAM_ATTR mode_change_event() 
{
  int_flg = MODE_CHANGE;
}

/** 
 * @fn    - mode_change_check()
 * @brief - 모드 변경 이벤트 처리
**/
void mode_change_check() 
{
  if (resource_release_flag == WIFI_RESOURCE_RELEASE) { //  Firebase 및 Wifi 자원 해제
      if (firebaseData != NULL) {
        Firebase.endStream(*firebaseData);
        Firebase.end(*firebaseData);
        delete firebaseData;
        firebaseData = NULL;
      }
      wifi_close();
      resource_release_flag = 0;
  }
  if ((!autoNextPageMode)) { //  디스플레이 수동 페이지 전환 모드 일 경우
    if (getModeNum(modeNum) == MODE_DISPLAY)  return;
    else if (get_critical_status() == BUSY_WAITS && digitalRead(MODE_PIN) == HIGH) {
      critical_section_end();
      int_flg = 0;
    }
  }
  if (preventContinuousClicks == true) {
    if (digitalRead(MODE_PIN) == HIGH) {
      int_flg = 0;
      preventContinuousClicks = false;
    }
    return;
  }
  if (get_critical_status() == NON_BUSY) 
  {
    if (int_flg == MODE_CHANGE)
    {
      if (g_board_config.model_info == VOICE_MODEL)
        audioTimerStop();

      mode_entry_init(getModeNum(modeNum));
      modeNum = ++modeNum % modeCount;
      modeStep = 0;
      int_flg = 0;
      g_initModeStep = LOOP_MODE_EXCUTE;
      categoryActionFlag = false;
      ledpannel.set_action_off();
    }
  }
}

/** 
 * @fn    - getModeNum()
 * @brief - 사용되는 모드 리스트안의 모드 Get
 * @param modeNum - 검색 할 Mode Number  
**/
int getModeNum(uint8_t modeNum) 
{
  return useModeList[modeNum];
}

/** 
 * @fn    - getCurModeNum()
 * @brief - 현재 Mode Number 반환하는 함수
 * @return - 현재 Mode Number
**/
int getCurModeNum() 
{
  return useModeList[modeNum];
}

/** 
 * @fn    - mode_entry_init()
 * @brief - 모드 변경 시 해당 모드의 자원 해제
 * @param ModeNum - 변경 되기전의 모드 Num
 * @TCH : 모드 변경 자원 해제 
**/
void mode_entry_init (uint8_t modeNum) {
  matrix->clearScreen();
  switch (modeNum)
  {
  case MODE_BLUETOOTH :
    bluetooth.disconnect();
    isBlinkBTLogo = true;
    break;
  case MODE_DISPLAY :
    bluetooth.disconnect();
    free_multiPage();
    break;
  case MODE_LORA :
    bluetooth.disconnect();
    LoRa.end();
    break;
  case MODE_WIFI :
    /** 
     * Task 제거하는 부분이 있어 Flag를 통하여 호출된 Task와 다른 Task에서 제거하기 위하여 flag 작업 
    **/
    resource_release_flag = WIFI_RESOURCE_RELEASE;
    break;
  case MODE_UART :
    break;
  default:
    break;
  }
}

/** 
 * @fn    - read_nvs_board_info()
 * @brief - NVS (Preferences) 영역에서 Data Read
 * @details - Read Data ::: 로테이션 값, X,Y 좌표, 행정구역 코드 번호, 위치 동,지역, 로고 비트맵, 감마 보정 enable set
**/
bool read_nvs_board_info() {
  char * logoBuffer = NULL;

  prefs.begin(PREFS_NAME);
  size_t pageSize = 0;
  
  /*  Board Info Data Read  */ 
  pageSize = prefs.getBytesLength(PREFS_KEY_BOARD_INFO_PAGE);

  if (pageSize) {
    prefs.getBytes(PREFS_KEY_BOARD_INFO_PAGE, prefsBoardInfoArray, pageSize);
    g_rotationValue = prefsBoardInfoArray[BOARD_INFO_IDX_ROTATION];
    matrix->gamma_enable =  prefsBoardInfoArray[BOARD_INFO_IDX_GAMMA_ENABLE];
  }
  pageSize = 0;

  /*  Board Config Read NVS!  */ 
  pageSize = prefs.getBytesLength(PREFS_KEY_BOARD_CONFIG_PAGE);
  if (pageSize) {
    uint8_t _temp_buffer[pageSize];  
    // Serial.printf("pageSize Ok! : %d \n", pageSize);
    prefs.getBytes(PREFS_KEY_BOARD_CONFIG_PAGE, _temp_buffer, pageSize);

    g_board_config.model_info =  _temp_buffer[BOARD_INFO_IDX_MODEL_VALUE];
    g_board_config.matrix_width =  (_temp_buffer[BOARD_INFO_IDX_MATRIX_WIDTH_TOP_8BIT] << 8);
    g_board_config.matrix_width =  (g_board_config.matrix_width |_temp_buffer[BOARD_INFO_IDX_MATRIX_WIDTH_LOW_8BIT]);
    g_board_config.matrix_height = (_temp_buffer[BOARD_INFO_IDX_MATRIX_HEIGHT_TOP_8BIT] << 8);
    g_board_config.matrix_height = (g_board_config.matrix_height | _temp_buffer[BOARD_INFO_IDX_MATRIX_HEIGHT_LOW_8BIT]);
    g_board_config.flash_size =  _temp_buffer[BOARD_INFO_IDX_FLASH_MEMORY_SIZE];
  }

  // Serial.printf("g_board_config.model_info : %d \n", g_board_config.model_info);
  // Serial.printf("g_board_config.matrix_width : %d \n", g_board_config.matrix_width);
  // Serial.printf("g_board_config.matrix_height : %d \n", g_board_config.matrix_height);
  // Serial.printf("g_board_config.flash_size : %d \n", g_board_config.flash_size);

  /*  Logo Bitmap Pixel Read  */ 
  pageSize = prefs.getBytesLength(PREFS_KEY_LOGO_PAGE);

  if (pageSize) {
    logoBuffer = (char *) ps_malloc (pageSize);
    prefs.getBytes(PREFS_KEY_LOGO_PAGE, logoBuffer, pageSize);
  }

  pageSize = prefs.getBytesLength(PREFS_KEY_RANDOM_CONTENT_INFO_PAGE);

  if (pageSize) { //  랜덤 컨텐츠 설정값 read
    uint8_t _temp_buffer[pageSize];  
    prefs.getBytes(PREFS_KEY_RANDOM_CONTENT_INFO_PAGE, _temp_buffer, pageSize);

    g_lora_sub_mode_type = _temp_buffer[NVS_RANDOM_CONTENT_PAGE_TYPE_IDX];
    g_gage_count = _temp_buffer[NVS_RANDOM_CONTENT_PAGE_GAGE_COUNT_IDX];
    //  무게 인식 최소 값 Get
    g_weight_recognitition = 0;
    g_weight_recognitition =  g_weight_recognitition | _temp_buffer[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FIRST_DIGIT_IDX] << 24;
    g_weight_recognitition =  g_weight_recognitition | _temp_buffer[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_SECOND_DIGIT_IDX] << 16;
    g_weight_recognitition =  g_weight_recognitition | _temp_buffer[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_THIRD_DIGIT_IDX] << 8;
    g_weight_recognitition =  g_weight_recognitition | _temp_buffer[NVS_RANDOM_CONTENT_RECOGNITION_MIN_VALUE_FOUTH_DIGIT_IDX];

    Serial.printf("g_weight_recognitition : %d \n", g_weight_recognitition);
  }

  prefs.end();

  if (logoBuffer != NULL) {
    // matrix->begin();
    uint8_t cmd = logoBuffer[0];

    if (cmd == SET_TEXT) {
      ledpannel.set_text(&logoBuffer[1]);  // element 0 is cmd
    } 
    else if (cmd == SET_COLOR_TEXT) {
      ledpannel.set_color_text(&logoBuffer[1]);  // element 0 is cmd 
    }  
    else if (cmd == SET_PICTURE) {
      ledpannel.print_picture(&logoBuffer[1]);  // element 0 is cmd
    }
    free(logoBuffer);
    return true;
  } else return false;
}

/** 
 * @fn    - init_modeChange_flag()
 * @brief - 모드 변경 값 변수 0 초기화
**/
void init_modeChange_flag() {
  int_flg = 0;
}

/** 
 * @fn    - set_transmit_mode_change()
 * @brief - uart <-> bluetooth 모드 변경
**/
void set_transmit_mode_change(char* data) {  //  Switch Mode to Communication
  uint8_t mode_info = data[0];
  if (mode_info == TRANSMIT_MODE_BLUETOOTH) {
    bool checkUpdate = false;
    mode_entry_init(getModeNum(modeNum));
    getSettingInfo(&modeNum, &ledDivLevel, &modeCount, useModeList, sizeof(useModeList), &checkUpdate, &startModeNum, &logoTime, &displayPageTime);
    useModeList[0] = MODE_BLUETOOTH;
    useModeList[1] = MODE_DISPLAY;
    modeCount = 2;
    int_flg = MODE_CHANGE;
    modeNum = -1;
  } else if (mode_info == TRANSMIT_MODE_UART) {
    mode_entry_init(getModeNum(modeNum));
    useModeList[0] = MODE_UART;
    useModeList[1] = MODE_BLUETOOTH;
    useModeList[2] = MODE_DISPLAY;
    modeCount = 3;
    int_flg = MODE_CHANGE;
    modeNum = -1;
  }
}
/**************************************************************************/
/** 
 * @fn      - set_mode_change(char* data)
 * @brief   - 모드 변경 함수
 * @details - 기존에 모드가 없다면 useModeList에 저장 후 플래시 삽입
 * @param data - 변경할 모드 정보
*/
/**************************************************************************/
void set_mode_change(char* data) {
  Serial.println("set_mode_change");
  mode_entry_init(getModeNum(modeNum));

  uint8_t _mode_num = data[0];
  int_flg = 0;
  modeStep = 0;
  categoryActionFlag = false;
  ledpannel.set_action_off();
  
  bool check_mode_already_exist = false;
  int mode_idx = 0;

  uint8_t buffer[SETTING_DATA_COUNT];
  int i = 0;
  int readBufferIndex = 0;

  ledpannel.set_matrix_end();

  readBufferIndex = readFile(LittleFS, "/setting.txt", buffer);
  i = SETTING_IDX_DP_CHECK; //  플래시 모드 정보 시작 인덱스

  for (int i = 0; i < MODE_COUNT; i++) {  //  payload로 넘어온 모드가 기존에 있는지 Check
    if (useModeList[i] == _mode_num) {
      check_mode_already_exist = true;
      mode_idx = i;
      break;
    }
  }

  if (!check_mode_already_exist) {  //  기존에 모드가 없다면 useModeList에 모드 추가
    // PRINTF("기존에 모드 없음");
    useModeList[modeCount] = _mode_num;
    modeNum = modeCount;
    modeCount++;
    buffer[i + (_mode_num - 1)] = 1;  //  Setting Idx 계산
  } else {
    modeNum = mode_idx;
  }
  
  writeFile(LittleFS, "/setting.txt", buffer, SETTING_DATA_COUNT);
  ledpannel.set_matrix_begin();
}

/**************************************************************************/
/** 
 * @fn      - set_mode_change_bt_non_disconnect()
 * @brief   - 블루투스 연결 끊지 않고 블루투스 모드로 변환하는 함수 
 * @details - 블루투스 모드 진입 시 연결 끊기므로 모드 스탭2로 이동하여 모드 변경 후 끊는 로직 넘김
*/
/**************************************************************************/
void set_mode_change_bt_non_disconnect() {
  // Serial.println("set_mode_change_bt_non_disconnect");
  switch (getModeNum(modeNum))
  {
  case  MODE_DISPLAY: 
    // Serial.println("displaymode");
    free_multiPage();
    break;
  case MODE_LORA :
    // Serial.println("LORA");
    bufferSerial.set_serial_type(TRANSMIT_BLUETOOTH);
    LoRa.end();
    break;
  default:
    break;
  } 

  modeNum = MODE_BLUETOOTH;
  modeStep = 2;

  ledpannel.matrixClear();   //  BT 모드 변환 시 화면 지움
}

/** 
 * @fn    - uartMode()
 * @brief - Tx Rx 유선 통신 모드
**/
void uartMode()
{
  if (modeStep == 0) {
    Serial.println("UART MODE!");

    bufferSerial.set_serial_type(TRANSMIT_UART_GPIO);
    if (!isCustomLogo) {
      String modeStr = "Uart";

      ledpannel.print_str_matrix_basic(1, modeStr, 0,  matrix->color565(0,15,0));
    }
    pre_time = millis();
    modeStep = 1;
  } 
  else if (modeStep == 1) {

    if (categoryActionFlag == true ) {  //  페이지 내용이 액션이라면 nextPageToken == true 다음 페이지 전환(한 번의 액션이 끝났을 때 true 반환하는 함수)
      if (nextPageToken == true) {
        ledpannel.set_action_off();
        modeStep = 2;        
      } 
    } 
    else {
    //  페이지 내용이 Default, Blink 라면 5초 후에 다음페이지 전환 
      if (delayExt(millis(), &pre_time, logoTime)) {
        fillScreenEx(0, 0, 0, matrix);
        modeStep = 2;        
      }
    }

    ledpannel.command_excute();
  }
  else if (modeStep == 2) 
  {
    if (g_board_config.model_info == ENT)
      bufferSerial.process(matrix, &ledpannel);
    
    ledpannel.command_excute();
  }
}

/** 
 * @fn    - check_flash_reset()
 * @brief - 버전 표시, 플래시 리셋 처리 함수
 * @details - 3초 누를 시 버전 표시, 10초 누를 시 플래시 초기화 및 재부팅
**/
void check_flash_reset() {  //  모드 스위치 3초 이상 누르고 있을 시 플래시 포멧 및 리셋  
  if (get_critical_status() == BUSY_WAITS) return;
  if (digitalRead(MODE_PIN) == LOW || isContinueClickedBtn)
  {
    if (!isContinueClickedBtn) {  //  Rest Loop 최초 진인지 체크 변수 -> 최초 진입이라면 시간 체크하고 탈출
      isContinueClickedBtn = true;
      pre_time_reset = millis();
      return;
    }

    if (digitalRead(MODE_PIN) == HIGH) {  //  최초 진입이 아닐 시 버튼클릭이 떼어져있다면 자원 초기화 하고 탈출
      isContinueClickedBtn = false;
      g_initModeStep = LOOP_MODE_EXCUTE;
      return;
    }

    int nextTime;
    if (g_initModeStep == LOOP_MODE_EXCUTE) nextTime = 3000; //  3000 == 버전 표시 버튼 클릭 시간 
    else if (g_initModeStep == LOOP_VERSION_PRINT) nextTime = 7000;  //  7000 == 초기화 버튼 클릭 시간 
    if (delayExt(millis(), &pre_time_reset, nextTime)) {
      if ((!autoNextPageMode) && getModeNum(modeNum) == MODE_DISPLAY) { //  디스플레이 수동모드인 경우 모드 전환
        mode_entry_init(getModeNum(modeNum));
        modeNum = ++modeNum % modeCount;
        modeStep = 0;
        categoryActionFlag = false;
        ledpannel.set_action_off();
        int_flg = 0;
        critical_section_begin();
      } else {
        if (g_initModeStep == LOOP_MODE_EXCUTE) { // 처음 3초 버전 출력
          ledpannel.print_str_matrix_basic(FONT_ONE_SIZE, FIRM_VERSION, CMD_ACT_DEFALUT, matrix->color565(0,255,0));
          g_initModeStep = LOOP_VERSION_PRINT;
          int_flg = 0;
          preventContinuousClicks = true;
          isBlinkBTLogo = false;
          // while (digitalRead(MODE_PIN) == LOW); //  Mode Pin Status Low case is Infinite loop . . .
        } else if (g_initModeStep == LOOP_VERSION_PRINT) {
          Serial.println("Flash Format & Reset");
          matrix->clearScreen();
          // String _str =  "";
          String _str = "초기화";
          ledpannel.print_str_matrix_basic(FONT_ONE_SIZE, _str, CMD_ACT_DEFALUT, matrix->color565(255,0,0));
          delay(3000);
          matrix->clearScreen();
          prefs.begin(PREFS_NAME);
          prefs.clear();
          // prefs.freeEntries();
          // prefs.remove();
          flashFormat();
          ESP.restart();
        }
      }
    } 
  } else return;
}


/** 
 * @fn    - timer_switch_print_page()
 * @brief - 디스플레이 페이지 시간 조건 체크 함수
 * @details - 3초 누를 시 버전 표시, 10초 누를 시 플래시 초기화 및 재부팅
 * @return PAGE_TIME_OK : 페이지 출력 (시간 조건 true)
 * @return PAGE_TIME_NO : 페이지 출력 안함(시간 조건 false)
**/
int timer_switch_print_page() {
  if (isTimeSynch == false) return PAGE_TIME_ASYNC;

  getLocalTime(&g_timeinfo);
  int curHour = g_timeinfo.tm_hour;
  int curMinute = g_timeinfo.tm_min;
  int curTotalMin = (curHour * 60) + curMinute;
  int startTotalMin = (timeSwitchStartHour[g_pageCount] * 60) + timeSwitchStartMinute[g_pageCount];
  int fromTotalMin = (timeSwitchFromHour[g_pageCount] * 60) + timeSwitchFromMinute[g_pageCount];
  // Serial.printf("startTotalMin <= fromTotalMin \n");
  // Serial.printf("curTotalMin : %d \n", curTotalMin);
  // Serial.printf("startTotalMin : %d \n", startTotalMin);
  // Serial.printf("fromTotalMin : %d \n", fromTotalMin);
  if (startTotalMin <= fromTotalMin) {
    if (startTotalMin <= curTotalMin && fromTotalMin >= curTotalMin) {
      return PAGE_TIME_OK;
    }
  } else {
    if (startTotalMin <= curTotalMin || fromTotalMin >= curTotalMin ) {
      return PAGE_TIME_OK;
    }
  }
  return PAGE_TIME_NO;
}

/** 
 * @fn    - streamCallback()
 * @brief - 파이어베이스 스트림 이벤트 콜백
 * @details - dataPath() == "data" 인 경우 통신 프로세스 처리
 *          - dataPath() == "renew" Resonse 값 전송
 * @param data - 파이어베이스에서 Read한 StreamData
**/
void streamCallback(StreamData data)
{
  Serial.println("streamCallback!!!!!!!!!!!!!!");
  // Serial.printf("sream path, %s\nevent path, %s\ndata type, %s\nevent type, %s\n\n",data.streamPath().
  // c_str(),data.dataPath().c_str(),data.dataType().c_str(), data.eventType().c_str());
  
  /*  페이지 넘기는 조건 초기화 */
  categoryActionFlag = false;
  nextPageToken = false;
  isStreamCallback = true;
  ledpannel.set_action_off();
  pre_time = millis();
  if (data.dataPath() == "/data") {
    streamDataString = data.stringData();
    bufferSerial.process(matrix, &ledpannel);

    // Serial.printf("streamDataString : %s \n", streamDataString );
    g_pageCount = -1;
    // streamSyncResponse();
  }
  if (data.dataPath() == "/renew")  streamSyncResponse();
}

/** 
 * @fn    - streamTimeoutCallback()
 * @brief - 파이어베이스 스트림 Time out (일정주기마다 호출 됨)
**/
void streamTimeoutCallback(bool timeout)
{
  Serial.println("Stream timeout, resume streaming...");
  if (timeout)
  {
    // Serial.println("Stream timeout, resume streaming...");
  }
}

/** 
 * @fn    - streamSyncResponse()
 * @brief - 파이어 베이스에 대한 응답 
**/
void streamSyncResponse() {
    char mac_str[13] = {0};
    uint8_t mac5[6];
    esp_read_mac(mac5, ESP_MAC_BT);

    sprintf(mac_str, "%x%x%x%x%x%x", mac5[0], mac5[1], mac5[2], mac5[3], mac5[4], mac5[5]);

    String strBtmac = "/";
    strBtmac.concat(mac_str);
    if ((Firebase.setTimestamp(*firebaseData, serverRequestUrl + "/" + mac_str)) == true) {
      Serial.println("Req Success!! ");
    } else {
      Serial.println("Req Fail!! ");
    }//  Write fail 
}

/** 
 * @fn    - device_init_start()
 * @brief - 상단 모서리 삼각표시 출력
**/
void device_init_start() {
  // matrix->begin();
  // 삳단모서리 6개 draw pixel
  matrix->drawPixel(0,0, matrix->color565(255,255,255));
  matrix->drawPixel(1,0, matrix->color565(255,255,255));
  matrix->drawPixel(0,1, matrix->color565(255,255,255));
  matrix->drawPixel(0,2, matrix->color565(255,255,255));
  matrix->drawPixel(1,1, matrix->color565(255,255,255));
  matrix->drawPixel(2,0, matrix->color565(255,255,255));
}

/** 
 * @fn    - log_ps_memory()
 * @brief - PSRAM Memory 출력
**/
void log_ps_memory() {
  log_d("Used PSRAM: %d", ESP.getPsramSize() - ESP.getFreePsram());
  log_d("ESP.getPsramSize() : %d", ESP.getPsramSize());
  log_d("ESP.getFreePsram() : %d", ESP.getFreePsram());
}

/** 
 * @fn    - log_heap_memory()
 * @brief - heap Memory 출력
**/
void log_heap_memory() {
  log_d("Total heap: %d", ESP.getHeapSize());
  log_d("Free heap: %d", ESP.getFreeHeap());
  log_d("Total PSRAM: %d", ESP.getPsramSize());
  log_d("Free PSRAM: %d", ESP.getFreePsram());
}
